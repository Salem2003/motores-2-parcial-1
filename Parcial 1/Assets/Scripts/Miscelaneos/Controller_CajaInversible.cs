using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_CajaInversible : MonoBehaviour
{
    public bool activo = false;
    private Inversion scriptInversion;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        scriptInversion = GetComponent<Inversion>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (activo)
        {
            scriptInversion.enabled = true;
            rb.useGravity = false;
        }
        else if (!activo)
        {
            scriptInversion.enabled = false;
            rb.useGravity = true;
        }
    }
}
