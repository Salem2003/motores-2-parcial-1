using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Proyectile : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Box") || other.gameObject.CompareTag("Untagged") || other.gameObject.CompareTag("Floor") || other.gameObject.CompareTag("Wall") || other.gameObject.CompareTag("Box") || other.gameObject.CompareTag("Door") || other.gameObject.CompareTag("Bridge") || other.gameObject.CompareTag("Platform"))
        {
            Destroy(gameObject);
        }
    }
}
