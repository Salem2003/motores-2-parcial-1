using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager i; //�nica instancia con un �nico punto global de acceso (Patr�n Singleton)

    void Awake()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject); //No se destruye al objeto si se cambia de escena

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>(); //Agrega un componente por c�digo
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sonido " + name + " no encontrado");
            return;
        }
        else
        {
            s.source.Play();
        }
    }

    public void Pause(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sonido " + name + " no encontrado");
            return;
        }
        else
        {
            s.source.Pause();
        }
    }
}
