using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("PJ3D"))
        {
            GameManager.indiceCamaras++;
            gameObject.SetActive(false);
        }
    }
}
