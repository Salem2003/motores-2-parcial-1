using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMat : MonoBehaviour
{
    public Material matTransparente;
    public Material matSolido;
    public int numPJ;

    void Update()
    {
        if (GameManager.actualPlayer == numPJ)
        {
            GetComponent<Renderer>().material = matSolido;
        }
        else
        {
            GetComponent<Renderer>().material = matTransparente;
        }
    }
}
