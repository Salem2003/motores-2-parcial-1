using UnityEngine;

[System.Serializable] //Permite que se visualicen las variables en el Inspector porque por m�s que est�n declaradas como Public, al no heredar de MonoBehaviour por defecto, no las visualiza
public class Sound
{
    public string name;
    public AudioClip clip;

    [Range(0f, 1f)] //Visualiza una barra de desplazamiento en el editor de 0 a 1 para que su modificaci�n sea m�s c�moda
    public float volume;

    [Range(0f, 1f)]
    public float pitch;

    public bool loop;

    [HideInInspector] //Esto es para que no muestre una variable en el editor aunque hayamos utilizado el System.Serializable
    public AudioSource source;
}