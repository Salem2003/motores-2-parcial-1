using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

public class MainMenu_Controller : MonoBehaviour
{
    public GameObject things;
    public GameObject controls;
    public TextMeshProUGUI txtON_OFF;

    void Start()
    {
        Time.timeScale = 0;
        txtON_OFF.text = $"{GameManager.i.audioBool}";
    }

    public void Play()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }

    public void Audio()
    {
        GameManager.i.audioBool = !GameManager.i.audioBool;
        txtON_OFF.text = $"{GameManager.i.audioBool}";
        GameManager.i.Audio();
    }

    public void Controls()
    {
        things.SetActive(false);
        controls.SetActive(true);
    }

    public void Back()
    {
        things.SetActive(true);
        controls.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }
}
