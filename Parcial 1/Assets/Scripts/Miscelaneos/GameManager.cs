﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager i;

    public static bool gameOver = false;

    public static bool winCondition = false;

    public static int actualPlayer = 0;

    //public List<Controller_Target> targets;

    public List<Controller_Player> players;

    public static int PJEnds = 0;

    private bool cambiarNivel = false;

    public static int nivel = 1;

    public GameObject background;

    public GameObject waterDown;

    public GameObject waterUp;

    public GameObject lvl1;

    public GameObject lvl2;

    public GameObject lvl3;

    public GameObject lvl4;

    public GameObject lvl5;

    public int cantTotalDeLvls;

    public static bool win = false;

    public static bool inViewMode = false;

    public static int indiceCamaras = 0;

    public List<GameObject> camaras;

    public GameObject camara;

    public GameObject Menu;

    public GameObject audioManager;

    public bool audioBool = true;

    void Awake()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject); //No se destruye al objeto si se cambia de escena
    }

    void Start()
    {
        Physics.gravity = new Vector3(0, -30, 0);
        actualPlayer = 0;
        PJEnds = 0;
        cambiarNivel = false;
        nivel = 1;
        gameOver = false;
        winCondition = false;
        win = false;
        indiceCamaras = 0;
        inViewMode = false;
        //SetConstraits();

        AudioManager.i.Play("BGM");
    }

    void Update()
    {
        Debug.Log(indiceCamaras);
        GetInput();

        if (PJEnds == 3)
        {
            PJEnds = 0;
            cambiarNivel = true;
            nivel += 1;
        }

        if (cambiarNivel && nivel != cantTotalDeLvls)
        {
            SetearNivel();
        }
        else if (cambiarNivel && nivel == cantTotalDeLvls)
        {
            win = true;
        }

        if (indiceCamaras > 0)
        {
            camaras[indiceCamaras - 1].SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene(0);

        if (Input.GetKeyDown(KeyCode.Escape)) { Menu.SetActive(true); Time.timeScale = 0; }
    }

    private void SetearNivel()
    {
        Controller_Hud.mostrarTxt = true;
        if (nivel == 2)
        {
            background.transform.position = new Vector3(160, 0, 100);
            waterDown.transform.position = new Vector3(160, 0.16f, 0);
            waterUp.transform.position = new Vector3(160, 15, 0);

            lvl2.gameObject.SetActive(true);
            lvl1.gameObject.SetActive(false);
        }
        else if (nivel == 3)
        {
            background.transform.position = new Vector3(315, 0, 100);
            waterDown.transform.position = new Vector3(315, 0.16f, 0);
            waterUp.transform.position = new Vector3(315, 15, 0);

            lvl3.gameObject.SetActive(true);
            lvl2.gameObject.SetActive(false);
        }
        else if (nivel == 4)
        {
            background.transform.position = new Vector3(520, 0, 100);
            waterDown.transform.position = new Vector3(520, 0.16f, 0);
            waterUp.transform.position = new Vector3(520, 15, 0);

            lvl4.gameObject.SetActive(true);
            lvl3.gameObject.SetActive(false);
        }
        else if (nivel == 5)
        {
            background.transform.position = new Vector3(650, 0, 100);
            waterDown.transform.position = new Vector3(650, 0.16f, 0);
            waterUp.transform.position = new Vector3(650, 15, 0);

            lvl5.gameObject.SetActive(true);
            lvl4.gameObject.SetActive(false);
        }
        cambiarNivel = false;
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.Q) && !inViewMode)
        {
            if (actualPlayer <= 0)
            {
                actualPlayer = 2;
                SetConstraits();
            }
            else
            {
                actualPlayer--;
                SetConstraits();
            }
        }
        if (Input.GetKeyDown(KeyCode.E) && !inViewMode)
        {
            if (actualPlayer >= 2)
            {
                actualPlayer = 0;
                SetConstraits();
            }
            else
            {
                actualPlayer++;
                SetConstraits();
            }
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            inViewMode = !inViewMode;

            if (inViewMode)
            {
                camara.SetActive(false);
                camaras[indiceCamaras].SetActive(true);
            }
            else
            {
                camaras[indiceCamaras].SetActive(false);
                camara.SetActive(true);
            }
        }
    }

    private void SetConstraits()
    {
        foreach (Controller_Player p in players)
        {
            if (p == players[actualPlayer] && p != players[players.Count - 1])
            {
                p.rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
            /*else
            {
                p.rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }*/
        }
    }

    public void Audio()
    {
        audioManager.SetActive(audioBool);
    }
}
