﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public TMP_Text txtSituacion;
    [HideInInspector] public static bool mostrarTxt = true;

    void Start()
    {
        txtSituacion.text = "Nivel 1";
        StartCoroutine(DesaparecerTexto(5));
    }

    void Update()
    {
        if (GameManager.gameOver)
        {
            txtSituacion.text = "Game Over";
        }
        if (GameManager.win)
        {
            txtSituacion.text = "You Win";
        }
        
        if (GameManager.nivel == 2 && mostrarTxt)
        {
            mostrarTxt = false;
            txtSituacion.text = "Nivel 2";
            StartCoroutine(DesaparecerTexto(5));
        }
        
        if (GameManager.nivel == 3 && mostrarTxt)
        {
            mostrarTxt = false;
            txtSituacion.text = "Nivel 3";
            StartCoroutine(DesaparecerTexto(5));
        }

        if (GameManager.nivel == 4 && mostrarTxt)
        {
            mostrarTxt = false;
            txtSituacion.text = "Nivel 4";
            StartCoroutine(DesaparecerTexto(5));
        }

        if (GameManager.nivel == 5 && mostrarTxt)
        {
            mostrarTxt = false;
            txtSituacion.text = "Nivel 5";
            StartCoroutine(DesaparecerTexto(5));
        }

        if (GameManager.PJEnds == 3)
        {
            mostrarTxt = true;
        }
    }

    private IEnumerator DesaparecerTexto(float time)
    {
        while (time > 0)
        {
            yield return new WaitForSeconds(1.0f);
            time--;
        }

        if (time <= 0)
        {
            txtSituacion.text = "";
        }
    }
}
