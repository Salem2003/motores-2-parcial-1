using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inversion : MonoBehaviour
{
    Rigidbody rb;
    public float fuerza;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb.AddForce(new Vector3(0, fuerza, 0));
    }
}
