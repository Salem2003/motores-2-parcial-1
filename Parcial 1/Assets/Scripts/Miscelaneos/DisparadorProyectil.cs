using UnityEngine;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;

public class DisparadorProyectil : MonoBehaviour
{
    public GameObject proyectil;
    public int velocidad;
    public float tiempoRestante;
    private GameObject pro;
    public float tiempoDestruccion;
    public float tiempoEnum;
    private bool disparar = false;
    public GameObject cabezaTorreta;

    void Start()
    {
        if (cabezaTorreta.GetComponent<Controller_TorretaFija>().activo)
        {
            StartCoroutine(ComenzarCuenta(tiempoRestante));
        }
    }

    void Update()
    {
        if (disparar && cabezaTorreta.GetComponent<Controller_TorretaFija>().activo)
        {
            disparar = false;
            GameObject pro = Instantiate(proyectil, transform.position + transform.up, transform.rotation);
            pro.GetComponent<Rigidbody>().AddForce(transform.up * velocidad, ForceMode.Impulse);
            Destroy(pro, tiempoDestruccion);
            StartCoroutine(ComenzarCuenta(tiempoRestante));
        }
    }

    private IEnumerator ComenzarCuenta(float valorCronometro)
    {
        while (valorCronometro > 0)
        {
            yield return new WaitForSeconds(tiempoEnum);
            valorCronometro--;
        }

        if (valorCronometro <= 0)
        {
            disparar = true;
        }
    }
}