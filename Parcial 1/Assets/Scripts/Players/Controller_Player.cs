﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    public float jumpForce = 10;

    public float speed = 5;

    public int playerNumber;

    public Rigidbody rb;

    internal bool canJump = true;

    public virtual void FixedUpdate()
    {
        if (GameManager.actualPlayer == playerNumber && !GameManager.inViewMode)
        {
            Movement();
        }
    }

    private void Update()
    {
        if (GameManager.actualPlayer == playerNumber && !GameManager.inViewMode)
        {
            Jump();
        }
    }

    public virtual void Movement()
    {
        if (Input.GetKey(KeyCode.A))
        {
                rb.velocity = new Vector3(1 * -speed , rb.velocity.y, 0);
        }
        else if (Input.GetKey(KeyCode.D))
        {
                rb.velocity = new Vector3(1 * speed, rb.velocity.y, 0);
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
    }

    public virtual void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (canJump)
            {
                canJump = false;
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("PJ3D") || collision.gameObject.CompareTag("Box") || collision.gameObject.CompareTag("Bridge"))
        {
            canJump = true;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            Destroy(this.gameObject);
            GameManager.gameOver = true;
        }
        if (other.gameObject.CompareTag("RedDeath"))
        {
            Destroy(this.gameObject);
            GameManager.gameOver = true;
        }
        if (other.gameObject.CompareTag("RedEnd"))
        {
            other.gameObject.GetComponent<Controller_End>().activado = true;
        }
    }
}
