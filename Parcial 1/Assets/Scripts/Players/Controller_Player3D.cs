using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player3D : Controller_Player
{
    public override void Movement()
    {
        if (GameManager.actualPlayer == playerNumber)
        {

            if (Input.GetKey(KeyCode.A))
            {
                transform.position -= new Vector3(speed, 0, 0) * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.D))
            {
                transform.position += new Vector3(speed, 0, 0) * Time.deltaTime;
            }
            else if(Input.GetKey(KeyCode.S))
            {
                transform.position -= new Vector3(0, speed, 0) * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.W))
            {
                transform.position += new Vector3(0, speed, 0) * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                transform.position += new Vector3(0, 0, speed) * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                transform.position -= new Vector3(0, 0, speed) * Time.deltaTime;
            }
        }
    }

    public override void Jump()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            Destroy(this.gameObject);
            GameManager.gameOver = true;
        }
        if (other.gameObject.CompareTag("BlueDeath"))
        {
            Destroy(this.gameObject);
            GameManager.gameOver = true;
        }
        if (other.gameObject.CompareTag("BlueEnd"))
        {
            other.gameObject.GetComponent<Controller_End>().activado = true;
        }
    }
}
