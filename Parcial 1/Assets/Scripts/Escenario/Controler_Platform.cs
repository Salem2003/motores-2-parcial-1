using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controler_Platform : MonoBehaviour
{
    private Transform pos;
    public GameObject luz1;
    public GameObject luz2;
    public Material luzApagada;
    public Material luzPrendida;
    public bool activo = false;
    public float posLimiteDer;
    public float posLimiteIzq;
    private bool derecha = true;
    public float speed;

    void Start()
    {
        pos = transform;
    }

    void Update()
    {
        if (activo)
        {
            luz1.GetComponent<Renderer>().material = luzPrendida;
            luz2.GetComponent<Renderer>().material = luzPrendida;
            if (derecha)
            {
                Debug.Log("der");
                transform.localPosition += new Vector3(speed, 0, 0) * Time.deltaTime;

                if (transform.localPosition.x > posLimiteDer)
                {
                    derecha = false;
                }
            }
            else if (!derecha)
            {
                Debug.Log("izq");
                transform.localPosition -= new Vector3(speed, 0, 0) * Time.deltaTime;

                if (transform.localPosition.x < posLimiteIzq)
                {
                    derecha = true;
                }
            }
        }
        else if (!activo)
        {
            luz1.GetComponent<Renderer>().material = luzApagada;
            luz2.GetComponent<Renderer>().material = luzApagada;
        }
    }
}
