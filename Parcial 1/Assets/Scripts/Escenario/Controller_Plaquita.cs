using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Plaquita : MonoBehaviour
{
    public GameObject ObjAAccionar;
    private bool presionado = false;
    public float posPlacaPresionada;
    public float posPlacaSinPresionar;
    private Transform posPlaca;
    private bool tocadoXPJ = false;
    private bool tocadoXPJ3D = false;
    private bool tocadoXCaja = false;
    private bool bajar = true;
    private bool subir = false;
    private Controller_CajaInversible scriptCajaInversible;
    public GameObject cable;
    public Material cablePrendido;
    public Material cableApagado;
    public bool activar;

    private bool cajaInversible = false;

    void Start()
    {
        posPlaca = transform;

        if (ObjAAccionar.CompareTag("Box"))
        {
            cajaInversible = true;
            scriptCajaInversible = ObjAAccionar.GetComponent<Controller_CajaInversible>();
        }
    }

    void Update()
    {
        if (presionado)
        {
            //cable.GetComponent<Renderer>().material = cablePrendido;
            if ((transform.localPosition.y > (posPlacaSinPresionar - 0.1f)) && bajar)
            {
                transform.localPosition -= new Vector3(0, 0.2f, 0) * Time.deltaTime;
            }
            else if (transform.localPosition.y <= (posPlacaSinPresionar - 0.1f) && bajar)
            {
                obj(activar);
                bajar = false;
            }
        }
        else if (!presionado)
        {
            //obj(false);
            //cable.GetComponent<Renderer>().material = cableApagado;
            bajar = true;
            if ((transform.localPosition.y < posPlacaSinPresionar) && subir)
            {
                transform.localPosition += new Vector3(0, 0.2f, 0) * Time.deltaTime;
            }
            else if (transform.localPosition.y >= posPlacaSinPresionar && subir)
            {
                subir = false;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            presionado = true;
            tocadoXPJ = true;
        }
        else if (collision.gameObject.CompareTag("PJ3D"))
        {
            presionado = true;
            tocadoXPJ3D = true;
        }
        else if (collision.gameObject.CompareTag("Box"))
        {
            presionado = true;
            tocadoXCaja = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && tocadoXPJ)
        {
            subir = true;
            presionado = false;
            tocadoXPJ = false;
            activar = !activar;
        }
        else if (collision.gameObject.CompareTag("PJ3D") && tocadoXPJ3D)
        {
            subir = true;
            presionado = false;
            tocadoXPJ3D = false;
            activar = !activar;
        }
        else if (collision.gameObject.CompareTag("Box") && tocadoXCaja)
        {
            subir = true;
            presionado = false;
            tocadoXCaja = false;
            activar = !activar;
        }
    }

    void obj(bool activar)
    {
        if (cajaInversible && activar)
        {
            scriptCajaInversible.activo = activar;
            cable.GetComponent<Renderer>().material = cablePrendido;
        }
        else if (cajaInversible && !activar)
        {
            scriptCajaInversible.activo = activar;
            cable.GetComponent<Renderer>().material = cableApagado;
        }
    }
}
