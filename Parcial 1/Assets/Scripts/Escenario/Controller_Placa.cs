using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Controller_Placa : MonoBehaviour
{
    public GameObject ObjAAccionar;
    private bool presionado = false;
    public float posPlacaPresionada;
    public float posPlacaSinPresionar;
    private Transform posPlaca;
    private bool tocadoXPJ = false;
    private bool tocadoXPJ3D = false;
    private bool tocadoXCaja = false;
    private bool bajar = true;
    private bool subir = false;
    private Controller_Puerta scriptPuerta;
    private Controller_PuertaRB scriptPuertaRB;
    private Controller_Bridge scriptPuente;
    private Controller_TorretaFija scriptTorretaFija;
    private Controler_Platform scriptPlataforma;
    public GameObject cable;
    public Material cablePrendido;
    public Material cableApagado;
    private bool puerta = false;
    private bool puertaRB = false;
    private bool puente = false;
    private bool torretaFija = false;
    private bool plataforma = false;

    void Start()
    {
        posPlaca = transform;

        if (ObjAAccionar.CompareTag("Door"))
        {
            puerta = true;
            scriptPuerta = ObjAAccionar.GetComponent<Controller_Puerta>();
        }
        else if (ObjAAccionar.CompareTag("Bridge"))
        {
            puente = true;
            scriptPuente = ObjAAccionar.GetComponent<Controller_Bridge>();
        }
        else if (ObjAAccionar.CompareTag("FixedTurret"))
        {
            torretaFija = true;
            scriptTorretaFija = ObjAAccionar.GetComponent<Controller_TorretaFija>();
        }
        else if (ObjAAccionar.CompareTag("Platform"))
        {
            plataforma = true;
            scriptPlataforma = ObjAAccionar.GetComponent<Controler_Platform>();
        }
        else if (ObjAAccionar.CompareTag("DoorRB"))
        {
            puertaRB = true;
            scriptPuertaRB = ObjAAccionar.GetComponent<Controller_PuertaRB>();
        }
    }

    void Update()
    {
        if (presionado)
        {
            obj(true);
            cable.GetComponent<Renderer>().material = cablePrendido;
            if ((transform.localPosition.y > (posPlacaSinPresionar - 0.1f)) && bajar)
            {
                transform.localPosition -= new Vector3(0, 0.2f, 0) * Time.deltaTime;
            }
            else if (transform.localPosition.y <= (posPlacaSinPresionar - 0.1f) && bajar)
            {
                bajar = false;
            }
        }
        else if (!presionado)
        {
            obj(false);
            cable.GetComponent<Renderer>().material = cableApagado;
            bajar = true;
            if ((transform.localPosition.y < posPlacaSinPresionar) && subir)
            {
                transform.localPosition += new Vector3(0, 0.2f, 0) * Time.deltaTime;
            }
            else if (transform.localPosition.y >= posPlacaSinPresionar && subir)
            {
                subir = false;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            presionado = true;
            tocadoXPJ = true;
        }
        else if (collision.gameObject.CompareTag("PJ3D"))
        {
            presionado = true;
            tocadoXPJ3D = true;
        }
        else if (collision.gameObject.CompareTag("Box"))
        {
            presionado = true;
            tocadoXCaja = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && tocadoXPJ)
        {
            subir = true;
            presionado = false;
            tocadoXPJ = false;
        }
        else if (collision.gameObject.CompareTag("PJ3D") && tocadoXPJ3D)
        {
            subir = true;
            presionado = false;
            tocadoXPJ3D = false;
        }
        else if (collision.gameObject.CompareTag("Box") && tocadoXCaja)
        {
            subir = true;
            presionado = false;
            tocadoXCaja = false;
        }
    }

    void obj(bool activar)
    {
        if (puerta)
        {
            scriptPuerta.activo = activar;
        }
        else if (puente)
        {
            scriptPuente.activo = activar;
        }
        else if (torretaFija)
        {
            scriptTorretaFija.activo = !activar;
        }
        else if (plataforma)
        {
            scriptPlataforma.activo = activar;
        }
        else if (puertaRB)
        {
            scriptPuertaRB.activo = activar;
        }
    }
}
