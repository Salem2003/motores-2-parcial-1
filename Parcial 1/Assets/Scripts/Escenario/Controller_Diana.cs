using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Diana : MonoBehaviour
{
    public GameObject poste;
    public GameObject diana;
    public float velocidadMovimiento;
    public float velocidadRotacion;
    public bool activo = false;
    private bool estallo = false;
    private bool bajar = false;
    private bool rotarDiana = false;
    public bool destruir = false;
    [SerializeField] private string tag;
    public GameObject ObjAAccionar;
    private Controller_Puerta scriptPuerta;
    private Controller_Bridge scriptPuente;
    private Controller_TorretaFija scriptTorretaFija;
    private Controler_Platform scriptPlataforma;
    public GameObject cable;
    public Material cablePrendido;
    public Material cableApagado;
    private bool puerta = false;
    private bool puente = false;
    private bool torretaFija = false;
    private bool plataforma = false;

    void Start()
    {
        if (ObjAAccionar.CompareTag("Door"))
        {
            puerta = true;
            scriptPuerta = ObjAAccionar.GetComponent<Controller_Puerta>();
        }
        else if (ObjAAccionar.CompareTag("Bridge"))
        {
            puente = true;
            scriptPuente = ObjAAccionar.GetComponent<Controller_Bridge>();
        }
        else if (ObjAAccionar.CompareTag("FixedTurret"))
        {
            torretaFija = true;
            scriptTorretaFija = ObjAAccionar.GetComponent<Controller_TorretaFija>();
        }
        else if (ObjAAccionar.CompareTag("Platform"))
        {
            plataforma = true;
            scriptPlataforma = ObjAAccionar.GetComponent<Controler_Platform>();
        }
    }

    void Update()
    {
        if (transform.localPosition.x <= 0.2f && !bajar)
        {
            transform.localPosition += new Vector3(velocidadMovimiento, 0, 0) * Time.deltaTime;

            if (transform.localPosition.x >= 0.2f)
            {
                bajar = true;
            }
        }
        else if (transform.localPosition.x >= -0.2f && bajar)
        {
            transform.localPosition -= new Vector3(velocidadMovimiento, 0, 0) * Time.deltaTime;

            if (transform.localPosition.x <= -0.2f)
            {
                bajar = false;
            }
        }

        if (destruir)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(tag) && !estallo)
        {
            estallo = true;
            activo = true;
            GetComponent<MeshRenderer>().enabled = false;
            Destroy(other.gameObject);

            transform.parent.GetComponent<Animator>().Play("DianaH");

            cable.GetComponent<Renderer>().material = cablePrendido;
            obj();
        }
    }

    void obj()
    {
        if (puerta)
        {
            scriptPuerta.activo = !scriptPuerta.activo;
        }
        else if (puente)
        {
            scriptPuente.activo = !scriptPuente.activo;
        }
        else if (torretaFija)
        {
            scriptTorretaFija.activo = !scriptTorretaFija.activo;
        }
        else if (plataforma)
        {
            scriptPlataforma.activo = !scriptPlataforma.activo;
        }
    }
}
