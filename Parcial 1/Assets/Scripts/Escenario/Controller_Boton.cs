using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

public class Controller_Boton : MonoBehaviour
{
    public GameObject ObjAAccionar;
    private bool presionado = false;
    public float posBotonSinPresionar;
    private Transform posBoton;
    private bool tocadoXPJ = false;
    private bool tocadoXPJ3D = false;
    private bool tocadoXCaja = false;
    private bool bajar = true;
    private Controller_Puerta scriptPuerta;
    private Controller_Bridge scriptPuente;
    private Controller_TorretaFija scriptTorretaFija;
    private Controler_Platform scriptPlataforma;
    public GameObject cable;
    public Material cablePrendido;
    public Material cableApagado;
    private bool puerta = false;
    private bool puente = false;
    private bool torretaFija = false;
    private bool plataforma = false;

    void Start()
    {
        posBoton = transform;

        if (ObjAAccionar.CompareTag("Door"))
        {
            puerta = true;
            scriptPuerta = ObjAAccionar.GetComponent<Controller_Puerta>();
        }
        else if (ObjAAccionar.CompareTag("Bridge"))
        {
            puente = true;
            scriptPuente = ObjAAccionar.GetComponent<Controller_Bridge>();
        }
        else if (ObjAAccionar.CompareTag("FixedTurret"))
        {
            torretaFija = true;
            scriptTorretaFija = ObjAAccionar.GetComponent<Controller_TorretaFija>();
        }
        else if (ObjAAccionar.CompareTag("Platform"))
        {
            plataforma = true;
            scriptPlataforma = ObjAAccionar.GetComponent<Controler_Platform>();
        }
    }

    void Update()
    {
        if (presionado)
        {
            cable.GetComponent<Renderer>().material = cablePrendido;
            obj(true);
            if ((transform.localPosition.y > (posBotonSinPresionar - 0.1f)) && bajar)
            {
                transform.localPosition -= new Vector3(0, 0.2f, 0) * Time.deltaTime;
            }
            else if (transform.localPosition.y <= (posBotonSinPresionar - 0.1f) && bajar)
            {
                bajar = false;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            presionado = true;
            tocadoXPJ = true;
        }
        else if (collision.gameObject.CompareTag("PJ3D"))
        {
            presionado = true;
            tocadoXPJ3D = true;
        }
        else if (collision.gameObject.CompareTag("Box"))
        {
            presionado = true;
            tocadoXCaja = true;
        }
    }

    void obj(bool activar)
    {
        if (puerta)
        {
            scriptPuerta.activo = activar;
        }
        else if (puente)
        {
            scriptPuente.activo = activar;
        }
        else if (torretaFija)
        {
            scriptTorretaFija.activo = !activar;
        }
        else if (plataforma)
        {
            scriptPlataforma.activo = activar;
        }
    }
}
