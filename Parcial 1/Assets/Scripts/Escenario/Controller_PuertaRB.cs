using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PuertaRB : MonoBehaviour
{
    [HideInInspector] public bool activo = false;
    [SerializeField] private float cerrada;
    [SerializeField] private float abierta;
    private bool abriendo = true;
    private bool cerrando = false;
    private Transform posOriginal;
    public bool invertida;
    //private BoxCollider bc;
    private Rigidbody rb;

    void Start()
    {
        posOriginal = transform;
        //bc = GetComponent<BoxCollider>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (invertida)
        {
            if (activo)
            {
                rb.excludeLayers = 0;
                //bc.enabled = true;
                if (transform.localPosition.y > abierta)
                {
                    transform.localPosition -= new Vector3(0, 5, 0) * Time.deltaTime;
                }

            }
            else if (!activo)
            {
                rb.excludeLayers = ~0;
                //bc.enabled = false;
                if (transform.localPosition.y < cerrada)
                {
                    transform.position += new Vector3(0, 5, 0) * Time.deltaTime;
                }

            }
        }
        else if (!invertida)
        {
            if (activo)
            {
                rb.excludeLayers = 0;
                //bc.enabled = true;
                if (transform.localPosition.y < abierta)
                {
                    transform.localPosition += new Vector3(0, 5, 0) * Time.deltaTime;
                }

            }
            else if (!activo)
            {
                rb.excludeLayers = ~0;
                //bc.enabled = false;
                if (transform.localPosition.y > cerrada)
                {
                    transform.position -= new Vector3(0, 5, 0) * Time.deltaTime;
                }

            }
        }

    }

    /*public void Activar()
    {
        activo = true;
    }

    public void Desactivar()
    {
        activo = false;
    }*/
}
