using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Puerta : MonoBehaviour
{
    [HideInInspector] public bool activo = false;
    [SerializeField] private float cerrada;
    [SerializeField] private float abierta;
    private bool abriendo = true;
    private bool cerrando = false;
    private Transform posOriginal;
    public bool invertida;

    void Start()
    {
        posOriginal = transform;
    }

    void Update()
    {
        if (invertida)
        {
            if (activo)
            {
                if (transform.localPosition.y > abierta)
                {
                    transform.localPosition -= new Vector3(0, 5, 0) * Time.deltaTime;
                }

            }
            else if (!activo)
            {
                if (transform.localPosition.y < cerrada)
                {
                    transform.position += new Vector3(0, 5, 0) * Time.deltaTime;
                }

            }
        }
        else if (!invertida)
        {
            if (activo)
            {
                if (transform.localPosition.y < abierta)
                {
                    transform.localPosition += new Vector3(0, 5, 0) * Time.deltaTime;
                }

            }
            else if (!activo)
            {
                if (transform.localPosition.y > cerrada)
                {
                    transform.position -= new Vector3(0, 5, 0) * Time.deltaTime;
                }

            }
        }

    }

    /*public void Activar()
    {
        activo = true;
    }

    public void Desactivar()
    {
        activo = false;
    }*/
}
