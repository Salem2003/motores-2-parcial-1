using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Bridge : MonoBehaviour
{
    [HideInInspector] public bool activo = false;
    [SerializeField] private float cerrada;
    [SerializeField] private float abierta;
    private bool abriendo = true;
    private bool cerrando = false;
    private Transform posOriginal;

    void Start()
    {
        posOriginal = transform;
    }

    void Update()
    {
        if (activo)
        {
            if ((transform.localPosition.x > abierta))
            {
                transform.localPosition -= new Vector3(5, 0, 0) * Time.deltaTime;
            }

        }
        else if (!activo)
        {
            if ((transform.localPosition.x < cerrada))
            {
                transform.localPosition += new Vector3(5, 0, 0) * Time.deltaTime;
            }

        }
    }

    /*public void Activar()
    {
        activo = true;
    }

    public void Desactivar()
    {
        activo = false;
    }*/
}
