using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_End : MonoBehaviour
{
    public GameObject bloqueo;
    public bool activado = false;

    void Update()
    {
        if (activado)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            bloqueo.SetActive(true);
            GameManager.PJEnds += 1;
            activado = false;
            gameObject.SetActive(false);
        }
    }
}
