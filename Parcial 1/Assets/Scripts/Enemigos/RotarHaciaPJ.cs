using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarHaciaPJ : MonoBehaviour
{
    //public float rapidez;
    public float velocidadRotacion;
    private Transform objetivo = null;
    //[SerializeField] private Rotador velRotY;
    private Controller_TorretaFija script;
    private bool rotar = true;

    void Start()
    {
        objetivo = GameObject.Find("PJAzul").transform;
        script = GetComponent<Controller_TorretaFija>();
    }

    void Update()
    {
        if (rotar)
        {
            Quaternion rotacion = Quaternion.LookRotation(objetivo.position - transform.position); //calculo el resultado de hasta donde debe rotar el objeto
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotacion, velocidadRotacion * Time.deltaTime); //roto lentamente al objeto en direccion a otro
        }

        if (!script.activo)
        {
            rotar = false;
        }
        else
        {
            rotar = true;
        }
    }
}
