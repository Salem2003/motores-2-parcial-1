using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_TorretaFija : MonoBehaviour
{
    public GameObject pj;
    //public GameObject proyectil;
    private Transform pos;
    public float speed;
    public bool activo = true;
    public Material luzPrendida;
    public Material luzApagada;
    public GameObject luz;

    void Start()
    {
        pos = transform;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (pos.position.y < pj.transform.position.y)
        {
            transform.position += new Vector3(0, speed, 0) * Time.deltaTime;
        }
        else if (pos.position.y > pj.transform.position.y)
        {
            transform.position -= new Vector3(0, speed, 0) * Time.deltaTime;
        }*/

        if (activo)
        {
            transform.position = new Vector3(pos.position.x, pj.transform.position.y, 0);
            luz.gameObject.GetComponent<Renderer>().material = luzPrendida;
        }
        else if (!activo)
        {
            luz.gameObject.GetComponent<Renderer>().material = luzApagada;
        }
    }
}
